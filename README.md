# EINSTEIN CONFIG

To run this need python3 installed on your computer

## Calling the config settings to read file in code
#### Import the config settings
    import config_settings

#### Load the config file
    config_settings.get_file_configs()
Optional to provide file path.  If no path provided user will be prompted to provide file
```
config_settings.get_file_configs('einstein_config.txt')
```

### User prompts:
##### Path to file
    Path to the file: einstein_config.txt
##### Get Config Value, type 'quit' to end
    Name of the config value to retrive: debug_mode
    quit to end

## Running the main file
##### In terminal on mac:
###### python3 main.py

```
Path to file
einstein_config.txt
Get Config Value, type 'quit' to end
user
Config Value: user
Config Value Type: <class 'str'>
Get Config Value, type 'quit' to end
debug_mode
Config Value: False
Config Value Type: <class 'bool'>
Get Config Value, type 'quit' to end
server_id
Config Value: 55331
Config Value Type: <class 'int'>
Get Config Value, type 'quit' to end
server_load_alarm
Config Value: 2.5
Config Value Type: <class 'float'>
Get Config Value, type 'quit' to end
quit
```